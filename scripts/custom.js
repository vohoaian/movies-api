Sugar.extend();
const api_key = 'ac4592cf2fa04e379677e474630177a7';
const img_path = 'https://image.tmdb.org/t/p/w500';

async function loadGenres() {
    // empty the list
    const main = $('#dropdown-genres');
    main.empty();
    // retrieve the list
    const res = await fetch(`https://api.themoviedb.org/3/genre/movie/list?api_key=${api_key}`);
    const genres = await res.json();
    // fill the list
    genres.genres.forEach(genre => {
        main.append(`
        <button onclick="loadMovies('genre', ${genre.id}, '${genre.name}')" class="dropdown-item">${genre.name}</button>
        `);
    });
}

async function loadMovies(type, arg, name, page = 1) {
    $('#main_show').show();
    $('#info_show').hide();
    $('#person_show').hide();
    $('#sidebar_show').show();
    $('#actor_show').hide();

    // empty the list
    const main = $('#main');
    main.empty();
    $('#pagination').empty();

    // change the title
    $('#main-title').text(name + ' Movies');
    // scroll to top with animation
    $("html, body").animate({ scrollTop: 0 }, "slow");
    // retrieve the list
    let url = 'https://api.themoviedb.org/3/';
    switch (type) {
        case 'genre':
            url += `discover/movie?api_key=${api_key}&with_genres=${arg}&page=${page}`;
            break;
        case 'trending':
            url += `trending/movie/${arg}?api_key=${api_key}&page=${page}`;
            break;
        case 'category':
            url += `movie/${arg}?api_key=${api_key}&page=${page}`
            break;
        case 'search':
            url += `search/movie?api_key=${api_key}&query=${arg}&page=${page}`
            break;
        case 'language':
            url += `discover/movie?api_key=${api_key}&with_original_language=${arg}&page=${page}`;
            break;
        case 'company':
            url += `discover/movie?api_key=${api_key}&with_companies=${arg}&page=${page}`;
            break;
        default:
            break;
    }
    // show loading image
    main.append(`<div class="spinner-border mx-auto text-success" role="status">
    <span class="sr-only">Loading...</span></div>`);
    const res = await fetch(url);
    const movies = await res.json();
    // hide loading image
    main.empty();
    // check empty
    if (movies.results.length == 0) {
        main.append(`
        <p class="m-3">
            No results.
        </p>`);
    }
    // fill the list
    movies.results.forEach(async function (movie) {
        const details = await getDetails(movie.id);
        main.append(`<div class="col-lg-3 col-md-4 col-sm-6 p-0 mb-3">${getMovieCard(details)}</div>`);
    });
    // show pagination
    getPagination(page, movies.total_pages, type, arg, name);
}

async function loadPersonalMovies(id, page = 1) {
    // empty the list
    const main = $('#movies_of');
    main.empty();
    // scroll to top with animation
    if (page == 1)
        $("html, body").animate({ scrollTop: 0 }, "slow");
    else
        $("html, body").animate({ scrollTop: main.offset().top }, "slow");
    // retrieve the list
    let url = 'https://api.themoviedb.org/3/' + `discover/movie?api_key=${api_key}&with_people=${id}&page=${page}`;

    // show loading image
    main.append(`<div class="spinner-border mx-auto text-success" role="status">
    <span class="sr-only">Loading...</span></div>`);
    const res = await fetch(url);
    const movies = await res.json();
    // hide loading image
    main.empty();
    // fill the list
    movies.results.forEach(async function (movie) {
        const details = await getDetails(movie.id);
        main.append(`<div class="col-lg-12 col-md-12 col-sm-12 px-1 mb-3">${getDetailCard(details, await getCredits(movie.id))}</div>`);
    });
    // show pagination
    getPersonalPagination(page, movies.total_pages, id);
}
async function loadActors(name, page = 1) {
    $('#actor_show').show();
    // empty the list
    const main = $('#actor_main');
    main.empty();
    $('#actor_pagination').empty();
    // change the title
    $('#actor-title').text('Movies of `' + name + '`');

    // scroll to top with animation
    if (page == 1)
        $("html, body").animate({ scrollTop: 0 }, "slow");
    else
        $("html, body").animate({ scrollTop: main.offset().top }, "slow");
    // retrieve the list
    let url = 'https://api.themoviedb.org/3/' + `search/person?api_key=${api_key}&query=${name}`;

    // show loading image
    main.append(`<div class="spinner-border mx-auto text-success" role="status">
    <span class="sr-only">Loading...</span></div>`);
    let res = await fetch(url);
    let people = await res.json();
    // continue to search movies
    if (people.results.length == 0) {
        main.empty();

        main.append(`
        <p class="m-3">
            No results.
        </p>`);
        return;

    }
    url = 'https://api.themoviedb.org/3/' + `discover/movie?api_key=${api_key}&with_people=${people.results[0].id}&page=${page}`;
    res = await fetch(url);
    const movies = await res.json();

    // hide loading image
    main.empty();
    // fill the list
    movies.results.forEach(async function (movie) {
        const details = await getDetails(movie.id);
        main.append(`<div class="col-lg-3 col-md-4 col-sm-6 p-0 mb-3">${getMovieCard(details)}</div>`);
    });
    // show pagination
    getActorPagination(page, movies.total_pages, name);
}
async function loadReviews(id, page = 1) {
    // empty the list
    const main = $('#reviews');
    main.empty();
    // scroll to top with animation
    if (page == 1)
        $("html, body").animate({ scrollTop: 0 }, "slow");
    else
        $("html, body").animate({ scrollTop: main.offset().top }, "slow");

    // show loading image
    main.append(`<div class="spinner-border mx-auto text-success" role="status">
    <span class="sr-only">Loading...</span></div>`);
    const reviews = await getReviews(id);
    // hide loading image
    main.empty();
    // check empty
    if (reviews.results.length == 0) {
        main.append(`
        <p class="border-bottom mt-3">
            No reviews.
        </p>`);
        return;
    }
    // fill the list
    reviews.results.forEach(async function (review) {
        main.append(`
        <p class="border-bottom mt-3">
            <span class="font-weight-bold text-success">${review.author}</span> : ${review.content}
        </p>`);
    });
    // show pagination
    getReviewPagination(page, reviews.total_pages, id);
}

async function loadPerson(id) {
    $('#main_show').hide();
    $('#info_show').hide();
    $('#person_show').show();
    $('#sidebar_show').hide();
    $('#actor_show').hide();

    $('#person-title').text('Person');

    // empty the list
    const person = $('#person');
    $('#person_profile').empty();
    $('#person_info').empty();
    $('#movies_of').empty();
    // scroll to top with animation
    $("html, body").animate({ scrollTop: 0 }, "slow");
    // show loading image
    person.prepend(`<div class="spinner-border mx-auto text-success" role="status">
    <span class="sr-only">Loading...</span></div>`);
    // get info
    let res = await getPerson(id);
    // change the title
    $('#person-title').text(res.name);
    // hide loading image
    person.children().first().remove();
    // Fill info
    $('#person_profile').html(`<img class="img-fluid d-block" src="${res.profile_path == null ? 'imgs/no-avatar.jpg' : img_path + res.profile_path}" alt="${res.name}" title="${res.name}">`);
    $('#person_info').html(`
    <li class="list-group-item border-0 bg-light">
        <h5 class="text-uppercase font-weight-bold text-success">${res.name}</h5></li>
    <li class="list-group-item">Know for department: <span class="font-weight-bold">${res.known_for_department}</span></li>
    <li class="list-group-item">Birthdate: ${res.birthday}</li>
    ${res.deathday == null ? '' : `<li class="list-group-item">Deathday: ${res.deathday}</li>`}
    <li class="list-group-item">Gender: ${res.gender == 1 ? 'Female' : 'Male'}</li>
    <li class="list-group-item">Place of birth: ${res.place_of_birth}</li>
    ${res.also_known_as.length == 0 ? '' : `<li class="list-group-item" id="person_aka">Also known as:</li>`}
    `);

    res.also_known_as.forEach((i, j) => $('#person_aka').append(` ${i}` + (j === res.also_known_as.length - 1 ? '' : ',')));
    $('#person_biography').html(res.biography);

    await loadPersonalMovies(id);
}

async function loadSideBar() {
    let url;
    let res;
    let movies;
    let details;
    // Load top rated
    url = `https://api.themoviedb.org/3/movie/top_rated?api_key=${api_key}`;
    res = await fetch(url);
    movies = await res.json();
    details = await getDetails(movies.results[0].id);
    $('#top_rated').html(`<div class="col-md">${getMovieCard(details)}</div>`);
    // Load latest
    url = `https://api.themoviedb.org/3/movie/latest?api_key=${api_key}`;
    res = await fetch(url);
    movies = await res.json();
    details = await getDetails(movies.id);
    $('#latest_movie').html(`<div class="col-md">${getMovieCard(details)}</div>`);
    // Load today_trending
    url = `https://api.themoviedb.org/3/trending/movie/day?api_key=${api_key}`;
    res = await fetch(url);
    movies = await res.json();
    for (let i = 0; i < 3; i++) {
        details = await getDetails(movies.results[i].id);
        $('#today_trending').append(`<div class="col-md">${getMovieCard(details)}</div>`);
    }
}

async function loadInfo(id) {
    const info = $('#info');
    $('#main_show').hide();
    $('#info_show').show();
    $('#person_show').hide();
    $('#sidebar_show').hide();
    $('#actor_show').hide();

    // scroll to top with animation
    $("html, body").animate({ scrollTop: 0 }, "slow");
    // clear
    $('#movie_poster').empty();
    $('#movie_info').empty();
    $('#movie_overview').empty();
    $('#reviews').empty();
    $('#similar_movies').empty();
    $('#info-title').text('Information');
    $('#carousel-actors .carousel-inner').empty();

    // show loading image
    info.prepend(`<div class="spinner-border mx-auto text-success" role="status">
    <span class="sr-only">Loading...</span></div>`);

    // load movie information
    let res = await getDetails(id);
    let credits = await getCredits(id);
    let similarMovies = await getSimilarMovies(id);
    // change the title
    $('#info-title').text(res.title);

    // hide loading image
    info.children().first().remove();

    // Fill info
    $('#movie_poster').html(`<img class="img-fluid d-block" src="${res.poster_path == null ? 'imgs/no-poster.jpg' : img_path + res.poster_path}" alt="${res.title}" title="${res.title}">`);
    $('#movie_info').html(`
    <li class="list-group-item border-0 bg-light">
        <h5 class="text-uppercase font-weight-bold text-success">${res.title}</h5></li>
    <li class="list-group-item">Original title: <span class="font-weight-bold">${res.original_title}</span></li>
    <li class="list-group-item">Release date: ${res.release_date}</li>
    <li class="list-group-item">Runtime: ${toHour(res.runtime)}</li>
    <li class="list-group-item">Vote: <kbd>${res.vote_average}</kbd> <small>(${res.vote_count} votes)</small></li>
    <li class="list-group-item" id="movie_director">Director:</li>
    <li class="list-group-item" id="movie_genre">Genre:</li>
    <li class="list-group-item" id="movie_language">Language:</li>
    <li class="list-group-item" id="movie_production_country">Production country: </li>
    <li class="list-group-item" id="movie_production_company">Production company: </li>`
    );

    res.genres.forEach((i, j) => $('#movie_genre').append(` <a href="" onclick="loadMovies('genre', ${i.id}, '${i.name}'); return false;">${i.name}</a>` + (j === res.genres.length - 1 ? '' : ',')));
    res.spoken_languages.forEach((i, j) => $('#movie_language').append(` <a href="" onclick="loadMovies('language', '${i.iso_639_1}', '${i.name}'); return false;">${i.name}</a>` + (j === res.spoken_languages.length - 1 ? '' : ',')));
    res.production_countries.forEach((i, j) => $('#movie_production_country').append(` ${i.name}` + (j === res.production_countries.length - 1 ? '' : ',')));
    res.production_companies.forEach((i, j) => $('#movie_production_company').append(` <a href="" onclick="loadMovies('company', ${i.id}, '${i.name}'); return false;">${i.name}</a>` + (j === res.production_companies.length - 1 ? '' : ',')));
    $('#movie_overview').html(res.overview);
    // Get director and actors
    let actors = credits.cast;
    let directors = [];
    credits.crew.forEach(function (i) {
        if (i.job === "Director")
            directors.push(i);
    });
    directors.forEach((i, j) => $('#movie_director').append(` <a href="" onclick="loadPerson(${i.id}); return false;">${i.name}</a>` + (j === directors.length - 1 ? '' : ',')));
    actors.forEach((i, j) => $('#carousel-actors .carousel-inner').append(`
    <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 ${j == 0 ? 'active' : ''}">
    <figure class="figure">
    <img src="${i.profile_path == null ? 'imgs/no-avatar.jpg' : img_path + i.profile_path}" class="img-fluid mx-auto d-block"
            alt="${i.name}" title="${i.name}" onclick="loadPerson(${i.id})" style="cursor: pointer;">
    <figcaption class="figure-caption text-center">${i.name}</figcaption>
    </figure>
    </div>
    `));
    // Reviews
    await loadReviews(id);

    // Get 8 first similar movies
    similarMovies = similarMovies.results.splice(0, 8);
    similarMovies.forEach(async function (movie) {
        const details = await getDetails(movie.id);
        $('#similar_movies').append(`<div class="col-lg-3 col-md-4 col-sm-6 px-1 mb-3">${getMovieCard(details)}</div>`);
    });

}

function search() {
    const query = $('#search_query').val();
    loadMovies('search', query, 'Search for `' + query + '`');
    loadActors(query);
}

$(document).ready(() => {
    loadMovies('category', 'now_playing', 'Now Playing');
    $('#search_query').keydown(function (e) {
        if (e.keyCode == 13) {
            search();
            return false;
        }
    });

    /*
        Carousel
    */
    $('#carousel-actors').on('slide.bs.carousel', function (e) {
        /*
            CC 2.0 License Iatek LLC 2018 - Attribution required
        */
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 5;
        var totalItems = $('.carousel-item').length;

        if (idx >= totalItems - (itemsPerSlide - 1)) {
            var it = itemsPerSlide - (totalItems - idx);
            for (var i = 0; i < it; i++) {
                // append slides to end
                if (e.direction == "left") {
                    $('.carousel-item').eq(i).appendTo('.carousel-inner');
                }
                else {
                    $('.carousel-item').eq(0).appendTo('.carousel-inner');
                }
            }
        }
    });
});




$(document).ready(loadGenres);

$(document).ready(loadSideBar);












/* Utilities */
async function getDetails(movie_id) {
    const res = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}?api_key=${api_key}`);
    const details = await res.json();
    return details;
}
async function getCredits(movie_id) {
    const res = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/credits?api_key=${api_key}`);
    const credits = await res.json();
    return credits;
}
async function getReviews(movie_id) {
    const res = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/reviews?api_key=${api_key}`);
    const reviews = await res.json();
    return reviews;
}
async function getSimilarMovies(movie_id) {
    const res = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/similar?api_key=${api_key}`);
    const movies = await res.json();
    return movies;
}
async function getPerson(id) {
    const res = await fetch(`https://api.themoviedb.org/3/person/${id}?api_key=${api_key}`);
    const person = await res.json();
    return person;
}
function getMovieCard(movie) {
    return `
    <div class="card h-100 mb-3">
    <img src="${movie.poster_path == null ? 'imgs/no-poster.jpg' : img_path + movie.poster_path}" class="card-img-top" alt="${movie.title}" title="${movie.title}" onclick="loadInfo(${movie.id})" style="cursor: pointer;">
    <div class="card-body">
            <h6 class="card-title movie-title" style="cursor: pointer;" onclick="loadInfo(${movie.id})" title="${movie.title}">${movie.title.truncateOnWord(25)}</h6>
            <p class="card-text small">${movie.overview.truncateOnWord(110)}</p>
        </div>
        <hr><p class="pl-2"><kbd>${movie.vote_average}</kbd> <small>(${movie.vote_count} votes)</small></p>
            </hr>
        <div class="card-footer">
            <small class="text-muted">${toHour(movie.runtime)}</small>
        </div>
    </div>`;
}
function getDetailCard(movie, credits) {

    function genre() {
        let res = '';
        movie.genres.forEach((i, j) => res += ` <a href="" onclick="loadMovies('genre', ${i.id}, '${i.name}'); return false;">${i.name}</a>` + (j === movie.genres.length - 1 ? '' : ','));
        return res;
    }
    function language() {
        let res = '';
        movie.spoken_languages.forEach((i, j) => res += ` <a href="" onclick="loadMovies('language', '${i.iso_639_1}', '${i.name}'); return false;">${i.name}</a>` + (j === movie.spoken_languages.length - 1 ? '' : ','));
        return res;
    }
    // Get director and actors
    let actors = credits.cast;
    let directors = [];
    credits.crew.forEach(function (i) {
        if (i.job === "Director")
            directors.push(i);
    });
    function director() {
        let res = '';
        directors.forEach((i, j) => res += ` <a href="" onclick="loadPerson(${i.id}); return false;">${i.name}</a>` + (j === directors.length - 1 ? '' : ','));
        return res;
    }
    function actor() {
        let res = '';
        actors.forEach((i, j) => j < 6 ? res += ` <a href="" onclick="loadPerson(${i.id}); return false;">${i.name}</a>` + (j === actors.length - 1 ? '' : ',') : null);
        return actors.length > 6 ? res + '...' : res;
    }
    return `
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
            <div class="col-md-4">
            <figure class="figure">
                <img src="${movie.poster_path == null ? 'imgs/no-poster.jpg' : img_path + movie.poster_path}" class="card-img-top" alt="${movie.title}" title="${movie.title}" onclick="loadInfo(${movie.id})" style="cursor: pointer;">
                <figcaption class="figure-caption text-center pt-3"><h5>${movie.title}</h5></figcaption>
            </figure></div>
            <div class="col-md-8">
                <ul class="list-group">
                    <li class="list-group-item">Release date: ${movie.release_date}</li>
                    <li class="list-group-item">Runtime: ${toHour(movie.runtime)}</li>
                    <li class="list-group-item">Vote: <kbd>${movie.vote_average}</kbd> <small>(${movie.vote_count} votes)</small></li>
                    <li class="list-group-item">Director:${director()}</li>
                    <li class="list-group-item">Genre:${genre()}</li>
                    <li class="list-group-item">Language:${language()}</li>        
                    <li class="list-group-item">Actors:${actor()}</li>
                    </ul>
            </div>
            <div class="row">
            <div class="col-md-12">
            <p class="px-3 small">${movie.overview.truncateOnWord(230)}</p>
            </div>
            </div>
            </div>
        </div>
    </div>`;
}
function getPersonCard(person) {
    return `
    <div class="card h-100 mb-3">
    <img src="${person.profile_path == null ? 'imgs/no-avatar.jpg' : img_path + person.profile_path}" class="card-img-top" alt="${person.name}" title="${person.name}" onclick="loadPerson(${person.id})" style="cursor: pointer;">
    <div class="card-body">
        <h6 class="card-title movie-title" style="cursor: pointer;" onclick="loadPerson(${person.id})" title="${person.name}">${person.name}</h6>
    </div>
    </div>`;
}
function toHour(min) {
    let hour = Math.floor(min / 60);
    let res = '';
    if (hour != 0)
        res = hour + ' hour' + (hour == 1 ? ' ' : 's ');
    let rest = min - hour * 60;
    if (min != 0)
        res += rest + ' minute' + (rest == 1 ? '' : 's');

    return res;
}
function getPagination(page, total_pages, type, arg, name) {
    const pagination = $('#pagination');
    pagination.empty();
    if (total_pages === 0) return false;

    // Them previous
    pagination.append(
        `<li class="page-item ${page == 1 ? 'disabled' : ''}">
        <button class="page-link" ${page == 1 ? '' : `onclick="loadMovies('${type}', '${arg}', '${name}', ${page - 1})"`}>
            Previous
        </button>
    </li>`);
    // First
    if (page >= 3) {
        pagination.append(`
            <li class="page-item">
                <button class="page-link" onclick="loadMovies('${type}', '${arg}', '${name}', ${1})">
                    1
                </button>
            </li>
        `);
        if (page > 3)
            pagination.append(`
                <li class="page-item">
                    <span class="page-link">
                        ...
                    </span>
                </li>
            `);
    }

    let i = page - 1; // start
    let j = page + 1; // end
    if (i === 0) {
        i++;
        if (total_pages > 3)
            j++;
    }
    if (page === total_pages) {
        if (i >= 3)
            i--;
        j--;
        if (j > total_pages)
            j--;
    }
    for (; i <= j; i++) {
        pagination.append(page == i ? `
        <li class="page-item active" aria-current="page">
            <span class="page-link">
                ${i}
                <span class="sr-only">(current)</span>
            </span>
        </li>
        ` : `
            <li class="page-item">
                <button class="page-link" onclick="loadMovies('${type}', '${arg}', '${name}', ${i})">
                    ${i}
                </button>
            </li>
        `);
    }
    // Last
    if (page <= total_pages - 2) {
        if (page < total_pages - 2)
            pagination.append(`
                <li class="page-item">
                    <span class="page-link">
                        ...
                    </span>
                </li>
            `);
        pagination.append(`
            <li class="page-item">
                <button class="page-link" onclick="loadMovies('${type}', '${arg}', '${name}', ${total_pages})">
                    ${total_pages}
                </button>
            </li>
        `);
    }
    // Them Next
    pagination.append(
        `<li class="page-item ${page == total_pages ? 'disabled' : ''}">
        <button class="page-link" ${page == total_pages ? '' : `onclick="loadMovies('${type}', '${arg}', '${name}', ${page + 1})"`}>
            Next
        </button>
    </li>`);
}

function getReviewPagination(page, total_pages, id) {
    const pagination = $('#review_pagination');
    pagination.empty();
    if (total_pages === 0) return false;

    // Them previous
    pagination.append(
        `<li class="page-item ${page == 1 ? 'disabled' : ''}">
        <button class="page-link" ${page == 1 ? '' : `onclick="loadReviews(${id}, ${page - 1})"`}>
            Previous
        </button>
    </li>`);
    // First
    if (page >= 3) {
        pagination.append(`
            <li class="page-item">
                <button class="page-link" onclick="loadReviews(${id}, ${1})">
                    1
                </button>
            </li>
        `);
        if (page > 3)
            pagination.append(`
                <li class="page-item">
                    <span class="page-link">
                        ...
                    </span>
                </li>
            `);
    }

    let i = page - 1; // start
    let j = page + 1; // end
    if (i === 0) {
        i++;
        if (total_pages > 3)
            j++;
    }
    if (page === total_pages) {
        if (i >= 3)
            i--;
        j--;
        if (j > total_pages)
            j--;
    }
    for (; i <= j; i++) {
        pagination.append(page == i ? `
        <li class="page-item active" aria-current="page">
            <span class="page-link">
                ${i}
                <span class="sr-only">(current)</span>
            </span>
        </li>
        ` : `
            <li class="page-item">
                <button class="page-link" onclick="loadReviews(${id}, ${i})">
                    ${i}
                </button>
            </li>
        `);
    }
    // Last
    if (page <= total_pages - 2) {
        if (page < total_pages - 2)
            pagination.append(`
                <li class="page-item">
                    <span class="page-link">
                        ...
                    </span>
                </li>
            `);
        pagination.append(`
            <li class="page-item">
                <button class="page-link" onclick="loadReviews(${id}, ${total_pages})">
                    ${total_pages}
                </button>
            </li>
        `);
    }
    // Them Next
    pagination.append(
        `<li class="page-item ${page == total_pages ? 'disabled' : ''}">
        <button class="page-link" ${page == total_pages ? '' : `onclick="loadReviews(${id}, ${page + 1})"`}>
            Next
        </button>
    </li>`);
}


function getPersonalPagination(page, total_pages, id) {
    const pagination = $('#personal_pagination');
    pagination.empty();
    if (total_pages === 0) return false;

    // Them previous
    pagination.append(
        `<li class="page-item ${page == 1 ? 'disabled' : ''}">
        <button class="page-link" ${page == 1 ? '' : `onclick="loadPersonalMovies(${id}, ${page - 1})"`}>
            Previous
        </button>
    </li>`);
    // First
    if (page >= 3) {
        pagination.append(`
            <li class="page-item">
                <button class="page-link" onclick="loadPersonalMovies(${id}, ${1})">
                    1
                </button>
            </li>
        `);
        if (page > 3)
            pagination.append(`
                <li class="page-item">
                    <span class="page-link">
                        ...
                    </span>
                </li>
            `);
    }

    let i = page - 1; // start
    let j = page + 1; // end
    if (i === 0) {
        i++;
        if (total_pages > 3)
            j++;
    }
    if (page === total_pages) {
        if (i >= 3)
            i--;
        j--;
        if (j > total_pages)
            j--;
    }
    for (; i <= j; i++) {
        pagination.append(page == i ? `
        <li class="page-item active" aria-current="page">
            <span class="page-link">
                ${i}
                <span class="sr-only">(current)</span>
            </span>
        </li>
        ` : `
            <li class="page-item">
                <button class="page-link" onclick="loadPersonalMovies(${id}, ${i})">
                    ${i}
                </button>
            </li>
        `);
    }
    // Last
    if (page <= total_pages - 2) {
        if (page < total_pages - 2)
            pagination.append(`
                <li class="page-item">
                    <span class="page-link">
                        ...
                    </span>
                </li>
            `);
        pagination.append(`
            <li class="page-item">
                <button class="page-link" onclick="loadPersonalMovies(${id}, ${total_pages})">
                    ${total_pages}
                </button>
            </li>
        `);
    }
    // Them Next
    pagination.append(
        `<li class="page-item ${page == total_pages ? 'disabled' : ''}">
        <button class="page-link" ${page == total_pages ? '' : `onclick="loadPersonalMovies(${id}, ${page + 1})"`}>
            Next
        </button>
    </li>`);
}
function getActorPagination(page, total_pages, name) {
    const pagination = $('#actor_pagination');
    pagination.empty();
    if (total_pages === 0) return false;

    // Them previous
    pagination.append(
        `<li class="page-item ${page == 1 ? 'disabled' : ''}">
        <button class="page-link" ${page == 1 ? '' : `onclick="loadActors('${name}', ${page - 1})"`}>
            Previous
        </button>
    </li>`);
    // First
    if (page >= 3) {
        pagination.append(`
            <li class="page-item">
                <button class="page-link" onclick="loadActors('${name}', ${1})">
                    1
                </button>
            </li>
        `);
        if (page > 3)
            pagination.append(`
                <li class="page-item">
                    <span class="page-link">
                        ...
                    </span>
                </li>
            `);
    }

    let i = page - 1; // start
    let j = page + 1; // end
    if (i === 0) {
        i++;
        if (total_pages > 3)
            j++;
    }
    if (page === total_pages) {
        if (i >= 3)
            i--;
        j--;
        if (j > total_pages)
            j--;
    }
    for (; i <= j; i++) {
        pagination.append(page == i ? `
        <li class="page-item active" aria-current="page">
            <span class="page-link">
                ${i}
                <span class="sr-only">(current)</span>
            </span>
        </li>
        ` : `
            <li class="page-item">
                <button class="page-link" onclick="loadActors('${name}', ${i})">
                    ${i}
                </button>
            </li>
        `);
    }
    // Last
    if (page <= total_pages - 2) {
        if (page < total_pages - 2)
            pagination.append(`
                <li class="page-item">
                    <span class="page-link">
                        ...
                    </span>
                </li>
            `);
        pagination.append(`
            <li class="page-item">
                <button class="page-link" onclick="loadActors('${name}', ${total_pages})">
                    ${total_pages}
                </button>
            </li>
        `);
    }
    // Them Next
    pagination.append(
        `<li class="page-item ${page == total_pages ? 'disabled' : ''}">
        <button class="page-link" ${page == total_pages ? '' : `onclick="loadActors('${name}', ${page + 1})"`}>
            Next
        </button>
    </li>`);
}